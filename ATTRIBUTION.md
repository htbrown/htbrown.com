# Attribution Document
htbrown.com uses a few projects that are listed below.

## Fonts
- Outfit (headings): Copyright (c) 2021 The Outfit Project Authors; https://github.com/Outfitio/Outfit-Fonts.
- Roboto (body): Copyright (c) 2011 Google Inc.; https://github.com/googlefonts/roboto.
- Akar Icons (icons): Copyright (c) 2020 Arturo Wibawa; https://github.com/artcoholic/akar-icons-fonts.

Where fonts hosted on Google Fonts have been used, the "google webfonts helper" (https://github.com/majodev/google-webfonts-helper) has been used to host them locally.

## Images
- macosicons.com: Content property of their respective owners. Used for application icons of ice cubes, todoist, reeder 5 and neovim.
