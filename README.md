# htbrown.com
This is the source code for my personal website, [htbrown.com](https://htbrown.com). More specifically, this is
version 9, built using [11ty](https://www.11ty.dev) for simplicity and modernity instead of [Astro](https://astro.build) or [Jekyll](https://jekyllrb.com).

Here we go again; another build tool for my website.

## Running and Building
Before trying to do anything, make sure you've installed all the dependencies by running `npm i` or the approproate command with whichever package manager you prefer.

Once this is done, building should be as simple as running `npm run build` and running a developer web server can be done with `npm run watch`.

The difference between the `watch` and `start` commands is that, when using the latter, 11ty will re-bundle all files in the project every time something is changed. With the former, it will build incrementally (using the `--incremental` flag for 11ty's CLI).
