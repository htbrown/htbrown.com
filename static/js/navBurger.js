let burger = document.getElementById("navBurger");
let menu = document.getElementById("navMenu");

burger.addEventListener("click", () => {
    menu.classList.toggle("open");
    if (menu.classList.contains("open")) {
        burger.firstElementChild.className = "ai-cross";
    } else {
        burger.firstElementChild.className = "ai-three-line-horizontal";
    }
})
