const path = require("node:path");

const { eleventyImageTransformPlugin } = require("@11ty/eleventy-img");
const sass = require("sass");

module.exports = function (eleventyConfig) {
    eleventyConfig.addPlugin(eleventyImageTransformPlugin, {
        extensions: "html,md,hbs",
        formats: ["webp", "jpeg"],
        defaultAttributes: {
            loading: "lazy",
            decoding: "async"
        }
    });

    // passthrough public
    eleventyConfig.addPassthroughCopy("static");
    // passthrough favicon
    eleventyConfig.addPassthroughCopy("src/favicon.ico");
    // passthrough attribution
    eleventyConfig.addPassthroughCopy("ATTRIBUTION.md");

    // scss building
    eleventyConfig.addTemplateFormats("scss");
    eleventyConfig.addExtension("scss", {
        outputFileExtension: "css",
        compile: async function (inputContent, inputPath) {
            let parsed = path.parse(inputPath);
            let result = sass.compileString(inputContent, {
                loadPaths: [parsed.dir || ".", this.config.dir.includes],
                style: "compressed"
            });

            this.addDependencies(inputPath, result.loadedUrls)

            return async (data) => result.css;
        }
    });

    // shortcodes
    eleventyConfig.addShortcode("socialLink", function(social, page) {
        let classList = "nav-item social";
        if (social.link == page.url) classList = classList + " active";
        let attributeString = `href="${social.link}" class="${classList}" aria-label="${social.name}"`;
        if (social.attributes) {
            for (let attr in social.attributes) {
                attributeString += ` ${attr}=${social.attributes[attr]}`
            }
        }
        return `<a ${attributeString}><i class="${social.icon}" /></a>`
    })

    // filters
    eleventyConfig.addFilter("getPostYear", (dateString) => {
        let date = new Date(dateString);
        return date.getFullYear();
    })
    eleventyConfig.addFilter("postsByCategory", (posts, category) => {
        category = category.toLowerCase();
        let result = posts.filter(post => {
            let categories = post.data.categories.map(c => c.toLowerCase());
            return categories.includes(category);
        });
        return result;
    })
    eleventyConfig.addFilter("titlecase", (text) => {  // this is nasty
        switch (text) {
            case "os": return "OS"; break;
            case "cpu": return "CPU"; break;
            default:
                return text.replace(
                    /\w\S*/g,
                    t => t.charAt(0).toUpperCase() + t.substring(1).toLowerCase()
                );
                break;
        }        
    })
    
    // collections
    eleventyConfig.addCollection("categories", (collectionApi) => {
        let categories = new Set();
        let posts = collectionApi.getFilteredByTag("posts");
        posts.forEach(p => {
            let categoryList = p.data.categories;
            categoryList.forEach(c => categories.add(c));
        });
        return Array.from(categories);
    })

    return {
        dir: {
            input: "src",
            layouts: "_layouts",
            output: "build"
        }
    }
}
