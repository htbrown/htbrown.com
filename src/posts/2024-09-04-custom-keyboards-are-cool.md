---
layout: post.liquid
title: Custom Keyboards Are Cool
description: Shush; they are exciting.
categories: ["tech"]
---

I'm not a nerd, you are.

## My Keyboards

I have two "custom" keyboards now: a KBDFans KBD67 Lite (R3????) and a Bridge75 from Shortcut Studio. I also have a couple older keyboards that I'm now too cool to use :)

### KBD67 Lite

This was my first proper custom keyboard and the only one that I have built from a kit. Here are the specs:

- Plastic case in smoky black.
- Polycarbonate (I think??) plate.
- Gateron Yellow switches that came pre-lubed from the factory and are smooth enough.
- Some cheap keycaps I got from KBDFans. They aren't good.

I'm fond of this keyboard because it was the first I built properly; I'm not sure many would consider it particularly good. I do like the smoky black aesthetic for the keyboard case but if I end up playing about with it more I might pair it with [this set of keycaps](https://mechboards.co.uk/collections/keycaps/products/tai-hao-cubic-translucent-keycaps?variant=41773938966733). It sounds generally okay; it's more muted than my other keyboards, which isn't a bad thing, but it definitely sounds like a plastic keyboard and the cheap keycaps I have don't help with that.

### Bridge75

This is my most recent keyboard purchase and, ironically, it was cheaper than the KBD67 Lite. Here are the specs:

- Aluminium case in black.
- Polycarbonate plate.
- Princess Linear switches that also came pre-lubed. Some of the switches were a bit over-lubed but there were spares.
- The included set of keycaps which are much better than the others.

In my opinion, this keyboard sounds *lovely*. I have it mostly stock right now but am probably going to customise it in the future, again possibly using [these keycaps](https://www.mechmods.co.uk/collections/keycaps/products/milkyway-keys-tosh?variant=44447675416799) because the old Apple aesthetic is neat.

Two gripes so far: the switches are on the whole good, but as I mentioned before some were a little over-lubed and I don't have many spare and the VIA/Vial compatability (use Vial, it's open source) is less than perfect. Otherwise, for its price, it's a lovely keyboard.
