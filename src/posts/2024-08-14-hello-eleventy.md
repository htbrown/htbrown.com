---
layout: post.liquid
title: Hello, Eleventy!
description: Only took me a year.
categories: ["webdev", "meta"]
---

Woah. A new website. Finally.

## The Journey to Eleventy
In the seven-ish months (it feels like two years) it has taken me to settle on this website design with Eleventy, I've found myself playing with many different static site generators, which I'm just going to call SSGs for the sake of not repeating myself, and JavaScript frameworks. Here are my thoughts!

### Jekyll

> If you're interested in Jekyll, you can take a look at its website [here](https://jekyllrb.com).

My initial goal, aside from refreshing the look of my website, was to try and get away from Jekyll.

I love Jekyll; it deals with static HTML files, doesn't force unnecessary JavaScript onto website users and most importantly gives me, the web develoepr, complete control over the website output. It was the first SSG I chose, primarily down to GitHub's promotion of it for Pages at the time, and the transition from basic HTML pages to something that could do slightly more was as smooth as it could be. Jekyll was, and still is for many people, an incredibly easy way to make developer experience for writing a blog so much better.

But then we get to its problems. Jekyll feels old, and Ruby feels older. The primary reason I have been looking to move away from it is simply because getting Ruby to work has been a nightmare. I will install it fine on one machine, battle with it on another for a couple hours only to return to the machine I thought it worked on to find something has broken it there too. I got fed up with troubleshooting bundler installs. Having the opportunity to look at other web frameworks, I have also realised now that Jekyll really does not suit some types of website. It does have collections, yes, but the ability to modify how any of it works is effectively non-existent. Image optimisation is left entirely down to the developer. Sure, these are a few minor gripes that could probably be fixed with third-party plugins, but similar concepts elsewhere (looking at you, WordPress), have put me off the idea almost entirely.

To be clear, I still do think Jekyll is a good tool: it has served me very well. My little nitpicks with it have just given me an excuse to try something new, which is always fun!

### Astro

> If you're interested in Astro, you can take a look at its website [here](https://astro.build).

Astro wasn't the first JS framework I tried during my tinkering, but it was the first that stood out to me as having a genuine option for static, generated content which didn't act as a single web page. I had built almost an entire website with it (the source code of which you can look at [here](https://gitlab.com/htbrown/htbrown.com/-/tree/v8b?ref_type=heads)), and thoroughly enjoyed the process. I liked that it encouraged me to put things into components and I liked the fact I had the option to use whichever framework I knew best. SCSS worked pretty much out of the box, images were optimised for me and the file structure just made sense in my head. All-in-all, if I were making a more complex site (and maybe could make my mind up with design), I would probably choose Astro.

But here's where I come on to the downsides of Astro, even though there aren't many and many of them are probably down to my inexperience. I couldn't manage to get my head around what Astro wanted me to do with styles; I understood that the best-practice was to encapsulate as much as possible and keep styles to their components. In fact, I liked the ability to style one type of element in one component and style it completely differently in another, but then when I wanted to make a slight modification where a component was used in another I would have to make frustrating workarounds to get it to behave. Had I understood the model of encapsulating *everything* then maybe I would've been alright. Another issue was the fact it relied upon JavaScript, at least partially. This was a weird one: the majority of the website worked flawlessly without JavaScript, but I would have odd problems with styles not loading correctly when disabling scripts. Not a major problem, but weird nonetheless and it made me think about the complexity of my site. 

These, coupled with a few other seemingly Astro-specific concepts, made me decide that I would rather stick to something more simple for this site.

### Eleventy

> If you're interested in Eleventy, you can take a look at its website [here](https://11ty.dev).

And that brings me to Eleventy (or is it 11ty?). It certainly isn't perfect, but it's given me the motivation to finally finish this update of the site, which has jumped from version 6 to version 9 in one public iteration. Woo, procrastination!

If I could describe this build tool in a few words, it would be Jekyll but modern and customisable. There isn't quite as much hand-holding in the documentation for beginners, and things like posts aren't set up immediately for you, but once you have things the way you like them it just gets out of your way. I could go on about how the only JavaScript it ships is the JavaScript you choose to include, how there are first-party plugins available for image optimisation, how SCSS is an excellent example of its power in allowing many types of templating languages, but this section has become long enough.

### Honorable Mentions
- [Next.js](https://nextjs.org) - Next seems incredibly powerful for web development, it's just a bit unnecessary for a website like this.
- [Vue](https://vuejs.org) - Vue is similar to Next; I have more experience with it than React-based alternatives but it remains unnecessarily complex and reliant on JavaScript.
- [Hugo](https://gohugo.io) - Hugo seemed interesting, I just didn't spend enough time on it to learn it properly. Maybe a next venture.

## The End Result
...is the website you are looking at right now! Although it's taken a while to get a finished product, I think the time I've used has given me a greater understanding of the web and has enabled me to learn more about both web development frameworks and web design. I'm incredibly happy with how this website has turned out, at least until the next redesign. Feel free to look around!

It also marks my attempt at starting a proper blog, but as a way to write down my own thoughts instead of presenting something to a non-existent audience. Hopefully I can stick to it.
