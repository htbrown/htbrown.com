module.exports = [
    {
        name: "contact",
        link: "/contact/",
        icon: "ai-mention"
    },
    {
        name: "gitlab",
        link: "https://gitlab.com/htbrown",
        icon: "ai-gitlab-fill",
        attributes: { rel: "me", target: "_blank" }
    },
    {
        name: "mastodon",
        link: "https://mastodonapp.uk/@htbrown",
        icon: "ai-mastodon-fill",
        attributes: { rel: "me", target: "_blank" }
    }
]
