const pkg = require("../../package.json");

module.exports = () => {
    git_branch = require("child_process").execSync("git branch --show-current").toString().trim();
    git_commit = require("child_process").execSync("git rev-parse HEAD").toString().trim();

    return {
        version: pkg.version,
        description: "Hi, I'm Hayden Brown.",
        build: {
            dateStarted: new Date().toISOString(),
            platform: process.platform,
            nodeVersion: process.version,
            dependencies: pkg.dependencies
        },
        git: {
            commit: git_commit,
            shortCommit: git_commit.slice(0, 7),
            branch: git_branch == "master" ? "main" : git_branch
        }
    }
}
