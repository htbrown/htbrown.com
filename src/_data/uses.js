const exec = require("child_process").execSync

module.exports = {
    lastModified: new Date(exec("git log -1 --pretty='format:%ci' src/_data/uses.js", { encoding: "utf-8" })),
    devices: [
        {
            name: "macbook pro",
            icon: "ai-laptop-device",
            star: true,
            link: "https://everymac.com/systems/apple/macbook_pro/specs/macbook-pro-m3-max-14-core-cpu-30-core-gpu-14-late-2023-specs.html",
            specs: {
                chip: "Apple M3 Max 14C/30C",
                memory: "36GB LPDDR5",
                storage: "1TB",
                os: "macOS Sequoia",
            },
            comments: "Yes, this laptop is slightly overkill for me right now. Yes, I absolutely love it. It has replaced my desktop as my main computer."
        },
        {
            name: "custom pc",
            icon: "ai-desktop-device",
            star: false,
            specs: {
                cpu: "AMD Ryzen 5 5600",
                memory: "16GB DDR4-3600",
                storage: "256GB + 1TB",
                os: "Windows 11 (nasty)"
            },
            comments: "I also love this computer, despite Windows. Currently acting as a game streaming PC."
        },
        {
            name: "iphone 15",
            icon: "ai-mobile-device",
            star: true,
            link: "https://everymac.com/systems/apple/iphone/specs/apple-iphone-15-global-a3090-specs.html",
            specs: {
                chip: "Apple A16 Bionic",
                memory: "6GB of something",
                storage: "128GB",
                os: "iOS 18"
            },
            comments: "Does what a phone needs to do. It's also green :)"
        },
        {
            name: "ipad 9th generation",
            icon: "ai-tablet-device",
            star: false,
            link: "https://everymac.com/systems/apple/ipad/specs/apple-ipad-10-2-inch-9th-gen-2021-a2602-wi-fi-only-specs.html",
            specs: {
                chip: "Apple A13 Bionic",
                memory: "3GB of something",
                storage: "64GB",
                os: "iOS 18"
            },
            comments: "Readingggggggggggg."
        }
    ],
    applications: [
        {
            name: "firefox",
            imageUrl: "/img/applications/firefox.png",
            link: "https://firefox.com"
        },
        {
            name: "apple music",
            imageUrl: "/img/applications/apple_music.png"
        },
        {
            name: "iterm2",
            imageUrl: "/img/applications/iterm2.png",
            link: "https://iterm2.com"
        },
        {
            name: "neovim",
            imageUrl: "/img/applications/neovim.png",
            link: "https://gitlab.com/htbrown/dotfiles/-/tree/main/config/nvim?ref_type=heads"
        },
        {
            name: "obsidian",
            imageUrl: "/img/applications/obsidian.png",
            link: "https://obsidian.md"
        },
        {
            name: "zotero",
            imageUrl: "/img/applications/zotero.png",
            link: "https://www.zotero.org"
        },
        {
            name: "anki",
            imageUrl: "/img/applications/anki.png",
            link: "https://apps.ankiweb.net"
        },
        {
            name: "todoist",
            imageUrl: "/img/applications/todoist.png",
            link: "https://todoist.com"
        },
        {
            name: "outlook",
            imageUrl: "/img/applications/outlook.png"
        },
        {
            name: "1password",
            imageUrl: "/img/applications/1password.png",
            link: "https://1password.com"
        },
    ],
    services: [
        {
            name: "hetzner",
            icon: "ai-cloud",
            link: "https://hetzner.cloud"
        },
        {
            name: "microsoft 365",
            icon: "ai-envelope"
        }
    ],
    other: [
        "KBDFans KBD67 Lite with Gateron Yellow switches and a Bridge75.",
        "Numerous pens, including a LAMY Al Star, LAMY Vista and Waterman Expert (sadly no converters for the fountain pens yet).",
        "Apple Watch Series 6.",
        "Sennheiser HD 600, Bose QC35ii, FiiO FH3 and a FiiO BTR5 as a source. Also nothing ear (a) for wireless earbuds.",
        "Q Acoustics 2020i (got for £50 at a local charity shop!) and a Marantz receiver to power them.",
        "Canon EOS 550d with lenses from Canon and Tamron."
    ]
}
